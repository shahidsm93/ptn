
package com.example.ptn2.network.retrofit.model.subuser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.thoughtbot.expandablecheckrecyclerview.models.CheckedExpandableGroup;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.List;

public class Datum extends CheckedExpandableGroup {

    @SerializedName("subuser")
    @Expose
    private Subuser subuser = new Subuser();

    private int groupIndex = 0;

    //Transient
    private ArrayList<com.example.ptn2.network.retrofit.model.vehicle.Datum> vehicles = new ArrayList<>();

    private Boolean isCollapsed = true;
    private Boolean isChecked = false;

    public Datum(String title, List<com.example.ptn2.network.retrofit.model.vehicle.Datum> items) {
        super(title, items);
        this.vehicles = (ArrayList<com.example.ptn2.network.retrofit.model.vehicle.Datum>) items;
    }

    public Subuser getSubuser() {
        return subuser;
    }

    public void setSubuser(Subuser subuser) {
        this.subuser = subuser;
    }


    public ArrayList<com.example.ptn2.network.retrofit.model.vehicle.Datum> getVehicles() {
        return vehicles;
    }

    public void setVehicles(ArrayList<com.example.ptn2.network.retrofit.model.vehicle.Datum> vehicles) {
        this.vehicles = vehicles;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public Boolean getCollapsed() {
        return isCollapsed;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }

    public void setCollapsed(Boolean collapsed) {
        isCollapsed = collapsed;
    }

    @Override
    public void onChildClicked(int childIndex, boolean checked) {

    }

    public int getGroupIndex() {
        return groupIndex;
    }

    public void setGroupIndex(int groupIndex) {
        this.groupIndex = groupIndex;
    }
}

