package com.example.ptn2.network.retrofit.service;

import com.example.ptn2.network.retrofit.model.subuser.SubUserResponse;
import com.example.ptn2.network.retrofit.model.vehicle.VehicleResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface VehicleService {
    @GET("ptn/api/position.php")
    Call<VehicleResponse> getVehicles(@Query("accesskey") String accesskey, @Query("sub") String sub);
}
