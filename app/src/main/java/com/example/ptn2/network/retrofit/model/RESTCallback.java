package com.example.ptn2.network.retrofit.model;

public interface RESTCallback {
    void onSucces(Object obj);
    void onFailure(String message);
}
