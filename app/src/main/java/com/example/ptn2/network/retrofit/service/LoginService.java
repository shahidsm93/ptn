package com.example.ptn2.network.retrofit.service;

import com.example.ptn2.network.retrofit.model.login.LoginResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LoginService {
    @GET("amreli/api/login.php")
    Call<LoginResponse> login(@Query("accesskey") String accesskey, @Query("name") String name, @Query("pass") String pass);
}
