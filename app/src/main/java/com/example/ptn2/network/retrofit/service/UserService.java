package com.example.ptn2.network.retrofit.service;

import com.example.ptn2.network.retrofit.model.login.LoginResponse;
import com.example.ptn2.network.retrofit.model.subuser.SubUserResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface UserService {
    @GET("ptn/api/sub_user.php")
    Call<SubUserResponse> getSubUser(@Query("accesskey") String accesskey, @Query("userid") String userid);
}
