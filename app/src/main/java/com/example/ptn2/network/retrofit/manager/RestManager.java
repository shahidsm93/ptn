package com.example.ptn2.network.retrofit.manager;

import com.example.ptn2.network.retrofit.model.RESTCallback;
import com.example.ptn2.network.retrofit.model.login.LoginResponse;
import com.example.ptn2.network.retrofit.model.subuser.SubUserResponse;
import com.example.ptn2.network.retrofit.model.vehicle.VehicleResponse;
import com.example.ptn2.network.retrofit.service.LoginService;
import com.example.ptn2.network.retrofit.service.UserService;
import com.example.ptn2.network.retrofit.service.VehicleService;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestManager {

    Retrofit retrofit;

    public static String baseUrl = "http://151.106.17.246:8080/";
    private String accesskey = "12345";

    public RestManager() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS);

        OkHttpClient okHttpClient = builder.build();

        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .baseUrl(baseUrl)
                .build();
    }

    public void getLogin(String name, String pass, final RESTCallback callback) {

        LoginService service = retrofit.create(LoginService.class);

        Call<LoginResponse> call = service.login(accesskey, name, pass);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                callback.onSucces(response.body());
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                callback.onFailure(t.getMessage());
            }
        });
    }

    public void getSubUser(String userId, final RESTCallback callback) {
        UserService service = retrofit.create(UserService.class);

        Call<SubUserResponse> call = service.getSubUser(accesskey, userId);

        call.enqueue(new Callback<SubUserResponse>() {
            @Override
            public void onResponse(Call<SubUserResponse> call, Response<SubUserResponse> response) {
                callback.onSucces(response.body());
            }

            @Override
            public void onFailure(Call<SubUserResponse> call, Throwable t) {
                callback.onFailure(t.getMessage());
            }
        });
    }

    public void getVehicles(String sub, final RESTCallback callback) {
        VehicleService service = retrofit.create(VehicleService.class);

        Call<VehicleResponse> call = service.getVehicles(accesskey, sub);

        call.enqueue(new Callback<VehicleResponse>() {
            @Override
            public void onResponse(Call<VehicleResponse> call, Response<VehicleResponse> response) {
                callback.onSucces(response.body());
            }

            @Override
            public void onFailure(Call<VehicleResponse> call, Throwable t) {
                callback.onFailure(t.getMessage());
            }
        });
    }
}
