
package com.example.ptn2.network.retrofit.model.subuser;

import com.example.ptn2.network.retrofit.model.vehicle.Datum;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class Subuser {

    @SerializedName("users_id")
    @Expose
    private String usersId;
    @SerializedName("vnumber")
    @Expose
    private String vnumber;
    @SerializedName("login")
    @Expose
    private String login;

    public Subuser() {
    }

    public String getUsersId() {
        return usersId;
    }

    public void setUsersId(String usersId) {
        this.usersId = usersId;
    }

    public String getVnumber() {
        return vnumber;
    }

    public void setVnumber(String vnumber) {
        this.vnumber = vnumber;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

}
