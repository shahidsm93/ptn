
package com.example.ptn2.network.retrofit.model.vehicle;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import retrofit2.http.POST;

public class Positions implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("altitude")
    @Expose
    private String altitude;
    @SerializedName("course")
    @Expose
    private String course;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("other")
    @Expose
    private String other;
    @SerializedName("power")
    @Expose
    private String power;
    @SerializedName("speed")
    @Expose
    private String speed;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("valid")
    @Expose
    private String valid;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("AlarmStatus")
    @Expose
    private String alarmStatus;
    @SerializedName("imileage")
    @Expose
    private String imileage;
    @SerializedName("ikey")
    @Expose
    private String ikey;
    @SerializedName("ireason")
    @Expose
    private String ireason;
    @SerializedName("ireasoncode")
    @Expose
    private String ireasoncode;
    @SerializedName("syscode")
    @Expose
    private String syscode;
    @SerializedName("vehicle_id")
    @Expose
    private String vehicleId;
    @SerializedName("dtype")
    @Expose
    private String dtype;
    @SerializedName("chk1")
    @Expose
    private String chk1;
    @SerializedName("vlocation")
    @Expose
    private String vlocation;
    @SerializedName("overspeed")
    @Expose
    private String overspeed;
    @SerializedName("record_creation_time")
    @Expose
    private String recordCreationTime;
    @SerializedName("recorddate")
    @Expose
    private String recorddate;
    @SerializedName("name")
    @Expose
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAltitude() {
        return altitude;
    }

    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAlarmStatus() {
        return alarmStatus;
    }

    public void setAlarmStatus(String alarmStatus) {
        this.alarmStatus = alarmStatus;
    }

    public String getImileage() {
        return imileage;
    }

    public void setImileage(String imileage) {
        this.imileage = imileage;
    }

    public String getIkey() {
        return ikey;
    }

    public void setIkey(String ikey) {
        this.ikey = ikey;
    }

    public String getIreason() {
        return ireason;
    }

    public void setIreason(String ireason) {
        this.ireason = ireason;
    }

    public String getIreasoncode() {
        return ireasoncode;
    }

    public void setIreasoncode(String ireasoncode) {
        this.ireasoncode = ireasoncode;
    }

    public String getSyscode() {
        return syscode;
    }

    public void setSyscode(String syscode) {
        this.syscode = syscode;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getDtype() {
        return dtype;
    }

    public void setDtype(String dtype) {
        this.dtype = dtype;
    }

    public String getChk1() {
        return chk1;
    }

    public void setChk1(String chk1) {
        this.chk1 = chk1;
    }

    public String getVlocation() {
        return vlocation;
    }

    public void setVlocation(String vlocation) {
        this.vlocation = vlocation;
    }

    public String getOverspeed() {
        return overspeed;
    }

    public void setOverspeed(String overspeed) {
        this.overspeed = overspeed;
    }

    public String getRecordCreationTime() {
        return recordCreationTime;
    }

    public void setRecordCreationTime(String recordCreationTime) {
        this.recordCreationTime = recordCreationTime;
    }

    public String getRecorddate() {
        return recorddate;
    }

    public void setRecorddate(String recorddate) {
        this.recorddate = recorddate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected Positions(Parcel in) {
        id = in.readString();
        vehicleId = in.readString();
        name = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        time = in.readString();
    }

    public Positions() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(vehicleId);
        parcel.writeString(name);
        parcel.writeString(latitude);
        parcel.writeString(longitude);
        parcel.writeString(time);
    }

    public static final Creator<Positions> CREATOR = new Creator<Positions>() {
        @Override
        public Positions createFromParcel(Parcel in) {
            return new Positions(in);
        }

        @Override
        public Positions[] newArray(int size) {
            return new Positions[size];
        }
    };
}
