
package com.example.ptn2.network.retrofit.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("privilege")
    @Expose
    private String privilege;
    @SerializedName("login")
    @Expose
    private String login;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("userSettings_id")
    @Expose
    private String userSettingsId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("telephone")
    @Expose
    private String telephone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("notify")
    @Expose
    private String notify;
    @SerializedName("subacc_id")
    @Expose
    private String subaccId;
    @SerializedName("allowed_actions")
    @Expose
    private String allowedActions;
    @SerializedName("independent_exist")
    @Expose
    private String independentExist;
    @SerializedName("image")
    @Expose
    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrivilege() {
        return privilege;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserSettingsId() {
        return userSettingsId;
    }

    public void setUserSettingsId(String userSettingsId) {
        this.userSettingsId = userSettingsId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNotify() {
        return notify;
    }

    public void setNotify(String notify) {
        this.notify = notify;
    }

    public String getSubaccId() {
        return subaccId;
    }

    public void setSubaccId(String subaccId) {
        this.subaccId = subaccId;
    }

    public String getAllowedActions() {
        return allowedActions;
    }

    public void setAllowedActions(String allowedActions) {
        this.allowedActions = allowedActions;
    }

    public String getIndependentExist() {
        return independentExist;
    }

    public void setIndependentExist(String independentExist) {
        this.independentExist = independentExist;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
