package com.example.ptn2.app;

import com.example.ptn2.network.retrofit.model.login.User;
import com.example.ptn2.network.retrofit.model.vehicle.Datum;

import java.util.ArrayList;

public class AppInstance {
    public static User rootUser = new User();
    public static ArrayList<Datum> selectedVehicles = new ArrayList<>();
}
