package com.example.ptn2;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Animation animSequential,animCrossFadeOut,animCrossFadeIn;
    TextView txtSeq,txtseq1;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        animSequential = AnimationUtils.loadAnimation(getApplicationContext(),
//                R.anim.sequential);
//        animCrossFadeOut = AnimationUtils.loadAnimation(getApplicationContext(),
//                R.anim.fade_in);
//        animCrossFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
//                R.anim.slide_down);
        txtSeq=(TextView)findViewById(R.id.txt_seq);
//        txtSeq.startAnimation(animCrossFadeOut);

        new Handler().postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void run() {
                Intent myIntent = new Intent(MainActivity.this,
                        Login.class);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                startActivity(myIntent);
                finish();
            }
        }, 300);


    }
}
