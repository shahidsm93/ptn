package com.example.ptn2;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ptn2.activity.BaseActivity;
import com.example.ptn2.adapter.SubUserCheckChangedCallback;
import com.example.ptn2.adapter.SubUserClickCallback;
import com.example.ptn2.adapter.SubUserReyclerAdapter;
import com.example.ptn2.app.AppInstance;
import com.example.ptn2.listeners.RecyclerItemClickListener;
import com.example.ptn2.network.retrofit.model.RESTCallback;
import com.example.ptn2.network.retrofit.model.subuser.Datum;
import com.example.ptn2.network.retrofit.model.subuser.SubUserResponse;
import com.example.ptn2.network.retrofit.model.subuser.Subuser;
import com.example.ptn2.network.retrofit.model.vehicle.Positions;
import com.example.ptn2.network.retrofit.model.vehicle.VehicleResponse;
import com.example.ptn2.persistance.SharedPrefUtil;
import com.thoughtbot.expandablerecyclerview.listeners.GroupExpandCollapseListener;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;

public class Index extends BaseActivity implements RecyclerItemClickListener.OnItemClickListener, SubUserClickCallback, SubUserCheckChangedCallback {

    TextView name,userName,previ;
    RecyclerView recyclerView;
    EditText searchET;
    ImageView go, logout, reload;
    CheckBox selectAllCheck;

    ArrayList<Datum> subUsers = new ArrayList<Datum>();
    ArrayList<Datum> filteredSubUsers = new ArrayList<Datum>();

    SubUserReyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        name=findViewById(R.id.name);
        userName=findViewById(R.id.user_name);
        previ= findViewById(R.id.previ);
        recyclerView= findViewById(R.id.recycler_view);
        searchET = findViewById(R.id.search);
        go = findViewById(R.id.go);
        logout = findViewById(R.id.logout);
        reload = findViewById(R.id.reload);
        selectAllCheck = findViewById(R.id.select_all_check);

        name.setText(AppInstance.rootUser.getLogin());
        userName.setText(AppInstance.rootUser.getLogin());
        previ.setText("("+AppInstance.rootUser.getPrivilege()+")");

        recyclerView.setLayoutManager(new LinearLayoutManager(Index.this));
        adapter = new SubUserReyclerAdapter(Index.this, filteredSubUsers, Index.this, Index.this);
        recyclerView.setAdapter(adapter);

        adapter.setOnGroupExpandCollapseListener(new GroupExpandCollapseListener() {
            @Override
            public void onGroupExpanded(ExpandableGroup group) {
                int found = -1;
                for(int i=0; i<filteredSubUsers.size(); i++) {
                    if(filteredSubUsers.get(i).getSubuser().getUsersId().equalsIgnoreCase(group.getTitle())) {
                        found = i;
                    }
                }
                if(found>=0 &&  filteredSubUsers.get(found).getVehicles().size() <=0) {
                    getVehicles(found);
                }
            }

            @Override
            public void onGroupCollapsed(ExpandableGroup group) {

            }
        });

        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder b = new AlertDialog.Builder(Index.this);
                b.setMessage("Reload data?");
                b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getSubUsers();
                    }
                });
                b.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                b.create().show();
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder b = new AlertDialog.Builder(Index.this);
                b.setMessage("Are you sure you want to logout?");
                b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SharedPrefUtil.getInstance(Index.this).clearUser();
                        startActivity(new Intent(Index.this, Login.class));
                        finish();
                    }
                });
                b.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                b.create().show();
            }
        });

        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppInstance.selectedVehicles.clear();

                for(Datum su: filteredSubUsers){
                    for(com.example.ptn2.network.retrofit.model.vehicle.Datum v : su.getVehicles()){
                        if(v.getChecked()) {
                            AppInstance.selectedVehicles.add(v);
                        }
                    }
                }

                if(AppInstance.selectedVehicles.size() <= 0 ){
                    Toast.makeText(Index.this, "Select at least one vehicle.", Toast.LENGTH_SHORT).show();
                    return;
                }

                startActivity(new Intent(Index.this, MapsActivity.class));
            }
        });

        searchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    String text = editable.toString();
                    for (int i = 0; i < subUsers.size(); i++) {

                        Datum user = subUsers.get(i);

                        filteredSubUsers.get(i).getVehicles().clear();

                        for (com.example.ptn2.network.retrofit.model.vehicle.Datum v : user.getVehicles()) {

                            if (v.getPositions().getName().toLowerCase().contains(text.toLowerCase())) {

                                com.example.ptn2.network.retrofit.model.vehicle.Datum newV = new com.example.ptn2.network.retrofit.model.vehicle.Datum();

                                Positions p = new Positions();
                                p.setDeviceId(v.getPositions().getDeviceId());
                                p.setName(v.getPositions().getName());
                                p.setLatitude(v.getPositions().getLatitude());
                                p.setLongitude(v.getPositions().getLongitude());
                                p.setSpeed(v.getPositions().getSpeed());
                                p.setTime(v.getPositions().getTime());

                                newV.setPositions(p);

                                filteredSubUsers.get(i).getVehicles().add(newV);
                            }
                        }
                    }
                    recyclerView.getAdapter().notifyDataSetChanged();
                } catch (Exception e) {

                }
            }
        });

        selectAllCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                for (Datum u : filteredSubUsers) {
                    u.setChecked(b);
                    for(com.example.ptn2.network.retrofit.model.vehicle.Datum d : u.getVehicles()) {
                        d.setChecked(b);
                    }
                }
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        });

        getSubUsers();
    }

    private void getSubUsers() {

        filteredSubUsers.clear();
        subUsers.clear();

        progressDialog.show();

        restManager.getSubUser(AppInstance.rootUser.getId(), new RESTCallback() {
            @Override
            public void onSucces(Object obj) {

                progressDialog.dismiss();

                subUsers.clear();
                filteredSubUsers.clear();

                SubUserResponse r = (SubUserResponse) obj;

                for(int i=0; i<r.getData().size(); i++) {

                    Datum u = r.getData().get(i);

                    Datum newU = new Datum(u.getSubuser().getUsersId(), new ArrayList<com.example.ptn2.network.retrofit.model.vehicle.Datum>());

                    Subuser sU = new Subuser();
                    sU.setLogin(u.getSubuser().getLogin());
                    sU.setUsersId(u.getSubuser().getUsersId());
                    sU.setVnumber(u.getSubuser().getVnumber());

                    newU.setSubuser(sU);
                    newU.setGroupIndex(i);

                    subUsers.add(newU);

                    Datum newU1 = new Datum(u.getSubuser().getUsersId(), new ArrayList<com.example.ptn2.network.retrofit.model.vehicle.Datum>());

                    Subuser sU1 = new Subuser();
                    sU1.setLogin(u.getSubuser().getLogin());
                    sU1.setUsersId(u.getSubuser().getUsersId());
                    sU1.setVnumber(u.getSubuser().getVnumber());

                    newU1.setSubuser(sU1);
                    newU1.setGroupIndex(i);

                    filteredSubUsers.add(newU1);
                }

                adapter = new SubUserReyclerAdapter(Index.this, filteredSubUsers, Index.this, Index.this);
                recyclerView.setAdapter(adapter);
                if(filteredSubUsers.size() > 0) {
                    getVehicles(0);
                }
            }

            @Override
            public void onFailure(String message) {
                progressDialog.dismiss();

                Toast.makeText(Index.this, ""+message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onItemClick(View view, final int position) {

    }

    @Override
    public void onLongItemClick(View view, int position) {

    }

    @Override
    public void onCheckChanged(Datum subUser, Boolean checked) {
        subUser.setChecked(checked);
        for(com.example.ptn2.network.retrofit.model.vehicle.Datum d : subUser.getVehicles()) {
            d.setChecked(checked);
        }
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onUserClick(int position) {
        if(subUsers.get(position).getVehicles().isEmpty()) {
            getVehicles(position);
        } else {

            subUsers.get(position).setCollapsed(!filteredSubUsers.get(position).getCollapsed());
            filteredSubUsers.get(position).setCollapsed(!filteredSubUsers.get(position).getCollapsed());

            recyclerView.setAdapter(new SubUserReyclerAdapter(Index.this, filteredSubUsers, Index.this, Index.this));
        }
    }

    private void getVehicles(final int position) {
        progressDialog.show();
        restManager.getVehicles(filteredSubUsers.get(position).getSubuser().getUsersId(), new RESTCallback() {
            @Override
            public void onSucces(Object obj) {
                progressDialog.dismiss();

                VehicleResponse r = (VehicleResponse) obj;

                ArrayList<com.example.ptn2.network.retrofit.model.vehicle.Datum> vehicles = (ArrayList<com.example.ptn2.network.retrofit.model.vehicle.Datum>) r.getData();

                ArrayList<com.example.ptn2.network.retrofit.model.vehicle.Datum> vehicles1 = new ArrayList<>();
                ArrayList<com.example.ptn2.network.retrofit.model.vehicle.Datum> vehicles2 = new ArrayList<>();

                for(com.example.ptn2.network.retrofit.model.vehicle.Datum v : vehicles) {

                    com.example.ptn2.network.retrofit.model.vehicle.Datum newV = new com.example.ptn2.network.retrofit.model.vehicle.Datum();

                    Positions p = new Positions();
                    p.setDeviceId(v.getPositions().getDeviceId());
                    p.setName(v.getPositions().getName());
                    p.setLatitude(v.getPositions().getLatitude());
                    p.setLongitude(v.getPositions().getLongitude());
                    p.setSpeed(v.getPositions().getSpeed());
                    p.setTime(v.getPositions().getTime());

                    newV.setPositions(p);

                    vehicles1.add(newV);

                    com.example.ptn2.network.retrofit.model.vehicle.Datum newV1 = new com.example.ptn2.network.retrofit.model.vehicle.Datum();

                    Positions p1 = new Positions();
                    p1.setDeviceId(v.getPositions().getDeviceId());
                    p1.setName(v.getPositions().getName());
                    p1.setLatitude(v.getPositions().getLatitude());
                    p1.setLongitude(v.getPositions().getLongitude());
                    p1.setSpeed(v.getPositions().getSpeed());
                    p1.setTime(v.getPositions().getTime());

                    newV1.setPositions(p1);

                    vehicles2.add(newV1);
                }

                Subuser subuser1 = new Subuser();
                subuser1.setLogin(filteredSubUsers.get(position).getSubuser().getLogin());
                subuser1.setUsersId(filteredSubUsers.get(position).getSubuser().getUsersId());
                subuser1.setVnumber(filteredSubUsers.get(position).getSubuser().getVnumber());

                Datum d1 = new Datum("", vehicles1);
                d1.setCollapsed(false);
                d1.setSubuser(subuser1);
                d1.setGroupIndex(filteredSubUsers.get(position).getGroupIndex());

                Subuser subuser2 = new Subuser();
                subuser2.setLogin(filteredSubUsers.get(position).getSubuser().getLogin());
                subuser2.setUsersId(filteredSubUsers.get(position).getSubuser().getUsersId());
                subuser2.setVnumber(filteredSubUsers.get(position).getSubuser().getVnumber());

                Datum d2 = new Datum("", vehicles2);
                d2.setCollapsed(false);
                d2.setSubuser(subuser2);
                d2.setGroupIndex(filteredSubUsers.get(position).getGroupIndex());

                filteredSubUsers.set(position, d1);
                subUsers.set(position, d2);

                if(position < filteredSubUsers.size() - 1) {
                    getVehicles(position + 1);
                } else {
                    try {
                        adapter.notifyDataSetChanged();
                        for(int i=0; i<filteredSubUsers.size(); i++) {
                            if (i == 0) {
                                adapter.toggleGroup(i);
                            } else {
                                adapter.toggleGroup(filteredSubUsers.get(i - 1).getVehicles().size() + 1);
                            }
                        }
                    } catch (Exception e) {
                        Log.e("TOGGLE_EXC", e.toString());
                    }

                }
            }

            @Override
            public void onFailure(String message) {
                progressDialog.dismiss();
                Toast.makeText(Index.this, ""+message, Toast.LENGTH_SHORT).show();
            }
        });
    }
}

