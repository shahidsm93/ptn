package com.example.ptn2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ptn2.R;
import com.example.ptn2.network.retrofit.model.vehicle.Datum;

import java.util.ArrayList;

public class VehiclesReyclerAdapter extends RecyclerView.Adapter<VehiclesReyclerAdapter.ViewHolder> {

    Context context;
    ArrayList<Datum> vehicles;

    public VehiclesReyclerAdapter(Context context, ArrayList<Datum> vehicles) {
        this.context = context;
        this.vehicles = vehicles;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vehicle_map, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Datum vehicel  = vehicles.get(position);

        holder.vehicleName.setText(vehicel.getPositions().getName());
    }

    @Override
    public int getItemCount() {
        return vehicles.size();
    }

    class ViewHolder extends  RecyclerView.ViewHolder {

        TextView vehicleName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            vehicleName = itemView.findViewById(R.id.vehicle_name);
        }
    }
}
