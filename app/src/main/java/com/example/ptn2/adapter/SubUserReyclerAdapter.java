package com.example.ptn2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ptn2.Index;
import com.example.ptn2.R;
import com.example.ptn2.listeners.RecyclerItemClickListener;
import com.example.ptn2.network.retrofit.model.subuser.Datum;
import com.thoughtbot.expandablecheckrecyclerview.CheckableChildRecyclerViewAdapter;
import com.thoughtbot.expandablecheckrecyclerview.models.CheckedExpandableGroup;
import com.thoughtbot.expandablecheckrecyclerview.viewholders.CheckableChildViewHolder;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.ArrayList;

public class SubUserReyclerAdapter extends CheckableChildRecyclerViewAdapter<SubUserReyclerAdapter.SubUserViewHolder, SubUserReyclerAdapter.VehicleViewHolder> {

    Context context;
    ArrayList<Datum> users;
    SubUserClickCallback userCallback;
    SubUserCheckChangedCallback checkCallback;

    public SubUserReyclerAdapter(Context context, ArrayList<Datum> users, SubUserClickCallback userCallback, SubUserCheckChangedCallback checkCallback) {
        super(users);
        this.context = context;
        this.users = users;
        this.userCallback = userCallback;
        this.checkCallback = checkCallback;
    }

    @Override
    public SubUserViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_user, parent, false);

        return new SubUserViewHolder(view);
    }

    @Override
    public VehicleViewHolder onCreateCheckChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vehicle, parent, false);

        return new VehicleViewHolder(view);
    }

    @Override
    public void onBindCheckChildViewHolder(VehicleViewHolder holder, int position, CheckedExpandableGroup group, int childIndex) {
        final com.example.ptn2.network.retrofit.model.vehicle.Datum vehicel  = (com.example.ptn2.network.retrofit.model.vehicle.Datum) group.getItems().get(childIndex);

        holder.vehicleName.setText(vehicel.getPositions().getName());
        holder.speed.setText("Speed: "+vehicel.getPositions().getSpeed());
        holder.time.setText(vehicel.getPositions().getTime());

        holder.check.setOnCheckedChangeListener(null);
        holder.check.setChecked(vehicel.getChecked());
        holder.check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                vehicel.setChecked(b);
            }
        });
    }

    @Override
    public void onBindGroupViewHolder(SubUserViewHolder holder, final int position, ExpandableGroup group) {
        final Datum user = (Datum) group;
        holder.userName.setText(user.getSubuser().getLogin());
//        holder.userName.setText("PTN "+(user.getGroupIndex()+1));

        holder.vehicleCount.setText(user.getSubuser().getVnumber());
//
        holder.check.setOnCheckedChangeListener(null);
        holder.check.setChecked(user.getChecked());
        holder.check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                checkCallback.onCheckChanged(user, b);
            }
        });
    }

    class SubUserViewHolder extends GroupViewHolder {

        TextView userName, vehicleCount;
        RecyclerView recyclerView;
        LinearLayout layout;
        ImageView drop;
        CheckBox check;

        public SubUserViewHolder(@NonNull View itemView) {
            super(itemView);

            userName = itemView.findViewById(R.id.user_name);
            vehicleCount = itemView.findViewById(R.id.vehicle_count);
            recyclerView = itemView.findViewById(R.id.recycler_view);
            layout = itemView.findViewById(R.id.layout);
            check = itemView.findViewById(R.id.check);
            drop = itemView.findViewById(R.id.drop);
        }
    }

    class VehicleViewHolder extends CheckableChildViewHolder {

        TextView vehicleName, speed, total, time;
        CheckBox check;

        public VehicleViewHolder(@NonNull View itemView) {
            super(itemView);

            vehicleName = itemView.findViewById(R.id.vehicle_name);
            speed = itemView.findViewById(R.id.total1);
            total = itemView.findViewById(R.id.total);
            time = itemView.findViewById(R.id.time);
            check = itemView.findViewById(R.id.check);
        }

        @Override
        public Checkable getCheckable() {
            return check;
        }
    }
}
