package com.example.ptn2.adapter;

import com.example.ptn2.network.retrofit.model.subuser.Datum;

public interface SubUserCheckChangedCallback {

    void onCheckChanged(Datum subUser, Boolean checked);
}
