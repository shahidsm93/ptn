package com.example.ptn2.adapter;

public interface SubUserClickCallback {

    void onUserClick(int position);
}
