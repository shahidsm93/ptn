package com.example.ptn2.persistance;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.ptn2.network.retrofit.model.login.User;

public class SharedPrefUtil {

    private static SharedPrefUtil instance;
    private Context context;

    private SharedPrefUtil(Context context) {
        this.context = context;
    }

    public static SharedPrefUtil getInstance(Context context) {
        if(instance == null) {
            instance = new SharedPrefUtil(context);
        }
        return  instance;
    }

    public void saveUser(User user) {
        SharedPreferences preferences = context.getSharedPreferences("User", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("id", user.getId());
        editor.putString("login", user.getLogin());
        editor.putString("password", user.getPassword());
        editor.putString("prev", user.getPrivilege());
        editor.apply();
    }

    public User getUser() {

        SharedPreferences preferences = context.getSharedPreferences("User", Context.MODE_PRIVATE);

        if(preferences.getString("id", null)  != null) {
            User user = new User();
            user.setId(preferences.getString("id", ""));
            user.setLogin(preferences.getString("login", ""));
            user.setPassword(preferences.getString("password", ""));
            user.setPrivilege(preferences.getString("prev", ""));

            return user;
        } else {
            return  null;
        }
    }

    public void clearUser() {
        SharedPreferences preferences = context.getSharedPreferences("User", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("id");
        editor.remove("login");
        editor.remove("password");
        editor.remove("prev");
        editor.apply();
    }
}
