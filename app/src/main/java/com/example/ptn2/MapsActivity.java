package com.example.ptn2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ptn2.adapter.SubUserReyclerAdapter;
import com.example.ptn2.adapter.VehiclesReyclerAdapter;
import com.example.ptn2.app.AppInstance;
import com.example.ptn2.listeners.RecyclerItemClickListener;
import com.example.ptn2.network.retrofit.model.vehicle.Datum;
import com.example.ptn2.network.retrofit.model.vehicle.Positions;
import com.example.ptn2.network.retrofit.model.vehicle.VehicleResponse;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, MapboxMap.OnMapClickListener, PermissionsListener, RecyclerItemClickListener.OnItemClickListener {
    // variables for adding location layer
    private MapView mapView;
    private MapboxMap mapboxMap;
    // variables for adding location layer
    private PermissionsManager permissionsManager;
    private LocationComponent locationComponent;
    // variables for calculating and drawing a route
    private DirectionsRoute currentRoute;
    private static final String TAG = "DirectionsActivity";
    private NavigationMapRoute navigationMapRoute;
    public static ProgressDialog pDialog;
    public static String url;
    public FloatingActionButton button;

    ArrayList<Marker> markers = new ArrayList<>();

    ImageView vehicles;
    RecyclerView recyclerView;

    ArrayList<Datum> filteredVehicles = new ArrayList<>();

    //LocationTrack locationTrack;
    Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this,"pk.eyJ1Ijoic2FtYWRhc2lhbSIsImEiOiJjazFxN3VleWMwMGR4M2NzeGV2ODdmMGx4In0.6pCAvR-W0tlqtYJKzz_rSQ");
        setContentView(R.layout.maps_activity);
        mapView = findViewById(R.id.mapView);
        vehicles = findViewById(R.id.vehicles);
        recyclerView = findViewById(R.id.recycler_view);

        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        Collections.sort(AppInstance.selectedVehicles, new Comparator<Datum>() {
            @Override
            public int compare(Datum v1, Datum v2) {
                return v1.getPositions().getName().compareToIgnoreCase(v2.getPositions().getName());
            }
        });

        for(Datum d : AppInstance.selectedVehicles) {
            filteredVehicles.add(d);
        }

        VehiclesReyclerAdapter adapter = new VehiclesReyclerAdapter(MapsActivity.this, filteredVehicles);
        recyclerView.setLayoutManager(new GridLayoutManager(MapsActivity.this, 3));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(MapsActivity.this, recyclerView, MapsActivity.this));

        vehicles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewGroup.LayoutParams params = recyclerView.getLayoutParams();
                if(params.height == 0) {
                    params.height = 300;
                } else {
                    params.height = 0;
                }
                recyclerView.setLayoutParams(params);
            }
        });
    }

    @Override
    public void onItemClick(View view, final int position) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                markers.get(position).showInfoWindow(mapboxMap, mapView);
            }
        }, 1000);
        double lat = Double.parseDouble(AppInstance.selectedVehicles.get(position).getPositions().getLatitude());
        double lng = Double.parseDouble(AppInstance.selectedVehicles.get(position).getPositions().getLongitude());
        mapboxMap.setCameraPosition(new CameraPosition.Builder()
                .target(new LatLng(lat,lng))
                .zoom(15)
                .build());
    }

    @Override
    public void onLongItemClick(View view, int position) {

    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;

        mapboxMap.setStyle(getString(R.string.navigation_guidance_night), new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
//                enableLocationComponent(style);
                Intent intent = getIntent();

//                String end_lat = intent.getStringExtra("lat");
//                String end_long = intent.getStringExtra("lng");
//                String name = intent.getStringExtra("name");

//                double lat = Double.parseDouble(end_lat);
//                double lng = Double.parseDouble(end_long);
//                mapboxMap.setMinZoomPreference(5);

                LatLngBounds.Builder b = new LatLngBounds.Builder();

                for(Datum v : AppInstance.selectedVehicles) {

                    double lat = Double.parseDouble(v.getPositions().getLatitude());
                    double lng = Double.parseDouble(v.getPositions().getLongitude());

                    Marker m = mapboxMap.addMarker(new MarkerOptions()
                            .position(new LatLng(lat,lng))
                            .title(v.getPositions().getName()));

                    m.setSnippet(v.getPositions().getName());
                    markers.add(m);

                    b.include(new LatLng(lat,lng));
                }

              //  Icon icon = iconFactory.fromResource(R.drawable.ic_truck);
                //mapboxMap.addOnMapClickListener(MapsActivity.this);

                double lat = Double.parseDouble(AppInstance.selectedVehicles.get(0).getPositions().getLatitude());
                double lng = Double.parseDouble(AppInstance.selectedVehicles.get(0).getPositions().getLongitude());



                if(AppInstance.selectedVehicles.size() > 1) {
                    LatLngBounds latLngBounds = b.build();
                    mapboxMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 40));
                } else {
                    mapboxMap.setCameraPosition(new CameraPosition.Builder()
                        .target(new LatLng(lat,lng))
                        .zoom(10)
                        .build());
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        markers.get(0).showInfoWindow(mapboxMap, mapView);
                    }
                }, 1000);

            }
        });

    }



    @SuppressWarnings({"MissingPermission"})
    @Override
    public boolean onMapClick(@NonNull LatLng point) {


        return true;
    }





    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
// Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
// Activate the MapboxMap LocationComponent to show user location
// Adding in LocationComponentOptions is also an optional parameter
            try {
                locationComponent = mapboxMap.getLocationComponent();
                locationComponent.activateLocationComponent(this, loadedMapStyle);
                locationComponent.setLocationComponentEnabled(true);
            } catch (Exception e ) {

            }
// Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);
        } else {
            try {
                permissionsManager = new PermissionsManager(this);
                permissionsManager.requestLocationPermissions(this);
            } catch (Exception e) {

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent(mapboxMap.getStyle());
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private class insert_server extends AsyncTask<String ,Void, Void> {


        @Override
        protected Void doInBackground(String... url) {
            try {
                   URL urls = new URL(url[0]);
                URLConnection urlConnection = urls.openConnection();
                HttpURLConnection connection = null;
                if (urlConnection instanceof HttpURLConnection) {
                    connection = (HttpURLConnection) urlConnection;
                    System.out.println(urls);
                } else {
                    System.out.println("Please enter an HTTP URL.");

                }

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String urlString = "";
                String current;

                while ((current = in.readLine()) != null) {
                    urlString += current;
                }
                System.out.println(urlString);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            /**
             * Updating parsed JSON data into ListView
             * */

        }
    }

}