package com.example.ptn2.activity;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ptn2.Index;
import com.example.ptn2.network.retrofit.manager.RestManager;

public class BaseActivity extends AppCompatActivity {


    public ProgressDialog progressDialog;

    public RestManager restManager = new RestManager();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
    }
}
