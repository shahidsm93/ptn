package com.example.ptn2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.ptn2.activity.BaseActivity;
import com.example.ptn2.app.AppInstance;
import com.example.ptn2.network.retrofit.model.RESTCallback;
import com.example.ptn2.network.retrofit.model.login.Datum;
import com.example.ptn2.network.retrofit.model.login.LoginResponse;
import com.example.ptn2.persistance.SharedPrefUtil;


public class Login extends BaseActivity {

    EditText password;
    EditText username;
    Button login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        password = findViewById(R.id.password);
        username = findViewById(R.id.username);
        login = findViewById(R.id.login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressDialog.show();

                restManager.getLogin(username.getText().toString(), password.getText().toString(), new RESTCallback() {
                    @Override
                    public void onSucces(Object obj) {
                        progressDialog.dismiss();

                        String login = "";

                        LoginResponse r = (LoginResponse) obj;
                        for(Datum datum : r.getData()) {
                            login = datum.getUser().getLogin();
                            AppInstance.rootUser = datum.getUser();
                        }

                        if(login.equalsIgnoreCase(username.getText().toString())) {

                            SharedPrefUtil.getInstance(Login.this).saveUser(AppInstance.rootUser);

                            startActivity(new Intent(Login.this, Index.class));
                            finish();
                        } else {
                            Toast.makeText(Login.this, "Invalid username or password.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        progressDialog.dismiss();
                        Toast.makeText(Login.this, ""+message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        if(SharedPrefUtil.getInstance(this).getUser() != null) {
            AppInstance.rootUser = SharedPrefUtil.getInstance(this).getUser();
            startActivity(new Intent(Login.this, Index.class));
            finish();
        }

    }
}

